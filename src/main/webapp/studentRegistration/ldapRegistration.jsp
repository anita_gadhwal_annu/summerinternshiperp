<!DOCTYPE html>
<%@page import="postgreSQLDatabase.registration.Query"%>
<%@page import="java.util.*"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>IIIT KOTA | Dashboard</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
<script
	src="//cdnjs.cloudflare.com/ajax/libs/validate.js/0.9.0/validate.min.js"></script>
<link rel="stylesheet"
	href="../plugins/alertify/alertifyjs/css/alertify.min.css">
<link rel="stylesheet"
	href="../plugins/alertify/alertifyjs/css/themes/default.min.css">
<script src="../plugins/alertify/alertifyjs/alertify.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  
  <![endif]-->
<script>
	var constraints =

	{

		password : {
			presence : true,
			format : {
				pattern : /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{6,20}$/,
				message : function(value, attribute, validatorOptions,
						attributes, globalOptions) {
					return validate.format(" Not a valid password! Should have one lowercase, one uppercase, one special and one digit of length 6-20",
							{
								text : value
							});
				}
			}

		},
		repeat_password : {
			equality: {
	            attribute: "password",
	            message: "^The passwords does not match"
	          }

		}
	};
</script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<%@ include file="header.jsp"%>
 <!-- Left side column. contains the logo and sidebar -->
 <%@ include file="main-sidebar.jsp"%>

   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
					Student <small>Home</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#" class="active"><i class="fa fa-dashboard"></i>Home</a></li>

      </ol>
    </section>

    <!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<div class="box box-primary">
            <div class="box-header with-border">
									<%
										String rid = request.getSession().getAttribute("reg_id").toString();
									%>
									<h3 class="box-title">
										Registration ID :
										<%=rid%></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->

								<form role="form" method="POST" action="../RegisterStudent" id="ldap">

            <span><input type="hidden" name="ref_no" value=""></span>
              <div class="box-body">
                <div class="form-group">
                 <%
		          long reg_id = Long.parseLong(request.getSession().getAttribute("reg_id").toString());
												ArrayList<String> usernames = postgreSQLDatabase.registration.Query.getUsernameGenerationData(reg_id);
												Iterator<String> i = usernames.iterator();
		           String un;
												while (i.hasNext()) {
													un = (String) i.next();
                  %>
                  <div class="radio">
												<label> <input type="radio" name="username"
													id="optionsRadios1" value=<%=un%> checked required><%=un%>
                    </label>
                  </div>
											<%
												}
											%>

										</div>
                  
									<label>Password must contain An upper case character,lower case character, special character and a digit and must be of total length 6 to 20.</label> 	
                <div class="form-group">
												<label for="exampleInputPassword1"> Password</label> <input
													type="password" name="password" class="form-control"
													value="" placeholder="Password" required>
												<div class="messages"></div>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1"> Repeat Password</label>
												<input type="password" name="repeat_password"
													class="form-control" value="" placeholder="Repeat Password"
													required>
												<div class="messages"></div>
                </div>
									
              </div>

              <!-- /.box-body -->

              <div class="box-footer">
                <input type="submit" class="btn btn-primary" value="Register"></input>
              </div>
            </form>
          </div>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
	</section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
		<%@ include file="footer.jsp"%>
  <!-- Control Sidebar -->
		<%@ include file="control-sidebar.jsp"%>
 </div>
	<!-- ./wrapper -->

	<!-- jQuery 2.1.4 -->
	<script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<!-- jQuery UI 1.11.4 -->
	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
  $.widget.bridge('uibutton', $.ui.button);
	</script>
	<!-- Bootstrap 3.3.5 -->
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<!-- Slimscroll -->
	<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="../plugins/fastclick/fastclick.min.js"></script>
	<!-- AdminLTE App -->
	<script src="../dist/js/app.min.js"></script>

	<!-- AdminLTE for demo purposes -->
	<script src="../dist/js/demo.js"></script>

	<script>
		// Before using it we must add the parse and format functions
		// Here is a sample implementation using moment.js
		validate.extend(validate.validators.datetime, {
			// The value is guaranteed not to be null or undefined but otherwise it
			// could be anything.
			parse : function(value, options) {
				return +moment.utc(value);
			},
			// Input is a unix timestamp
			format : function(value, options) {
				var format = options.dateOnly ? "MM-DD-YYYY"
						: "YYYY-MM-DD hh:mm:ss";
				return moment.utc(value).format(format);
			}
		});

		// Hook up the form so we can prevent it from being posted
		var form = document.getElementById("ldap");
		form.addEventListener("submit", function(ev) {
			ev.preventDefault();
			handleFormSubmit(form);
		});
		// Hook up the inputs to validate on the fly
		var inputs = form.querySelectorAll("input, textarea, select")
		for (var i = 0; i < inputs.length; ++i) {
			inputs.item(i).addEventListener("change", function(ev) {
				var errors = validate(form, constraints) || {};

				showErrorsForInput(this, errors[this.name])
			});
		}

		function handleFormSubmit(form, input) {
			// validate the form aainst the constraints
			var errors = validate(form, constraints);
			// then we update the form to reflect the results

			if (!errors) {
				
				form.submit();

			} else {
				showErrors(form, errors || {});
				alertify.error('Check the form for errors!');
			}
		}

		// Updates the inputs with the validation errors
		function showErrors(form, errors) {
			// We loop through all the inputs and show the errors for that input
			_
					.each(
							form
									.querySelectorAll("input[name], select[name], textarea[name]"),
							function(input) {
								// Since the errors can be null if no errors were found we need to handle
								// that
								showErrorsForInput(input, errors
										&& errors[input.name]);
							});
		}

		// Shows the errors for a specific input
		function showErrorsForInput(input, errors) {
			// This is the root of the input
			// Find where the error messages will be insert into
			var formGroup = closestParent(input.parentNode, "form-group")
			// Find where the error messages will be insert into
			, messages = formGroup.getElementsByClassName("messages")[0];
			// First we remove any old messages and resets the classes

			resetFormGroup(formGroup);
			// If we have errors
			if (errors) {
				// we first mark the group has having errors
				formGroup.classList.add("has-error");
				// then we append all the errors
				_.each(errors, function(error) {
					addError(messages, error);
				});
			} else {
				// otherwise we simply mark it as success
				formGroup.classList.add("has-success");
			}
		}

		// Recusively finds the closest parent that has the specified class
		function closestParent(child, className) {
			if (!child || child == document) {
				return null;
			}
			if (child.classList.contains(className)) {
				return child;
			} else {
				return closestParent(child.parentNode, className);
			}
		}

		function resetFormGroup(formGroup) {
			// Remove the success and error classes
			formGroup.classList.remove("has-error");
			formGroup.classList.remove("has-success");
			// and remove any old messages
			_.each(formGroup.querySelectorAll(".help-block.error"),
					function(el) {
						el.parentNode.removeChild(el);
					});
		}

		// Adds the specified error with the following markup
		// <p class="help-block error">[message]</p>
		function addError(messages, error) {
			var block = document.createElement("p");
			block.classList.add("help-block");
			block.classList.add("error");
			block.innerText = error;
			messages.appendChild(block);
		}

		function showSuccess() {
			// We made it \:D/
			alert("Success!");
		}
	</script>
</body>
</html>