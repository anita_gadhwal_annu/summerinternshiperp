<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="postgreSQLDatabase.registration.Query"%>
    <%@page import="users.Student"%>
    <%@page import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" >
<style type="text/css">
@page{
size:A4;
}
table, tr, td, th {border:1px solid black; border-collapse:collapse;}
</style>
<title>Insert title here</title>
</head>
<body>
<div class="box-body" style="font-size:80%">
								<table id="example1" class="table table-bordered table-striped" >
									<thead style="font-size:120%">
										<tr>
											<th>Name</th>
											<th>JEE Main Roll No.</th>											<th>State</th>
											<th>Phone Number</th>
											<th>Email</th>
											<th>Program Allocated</th>
											<th>Allocated Category</th>
											<th>Physically Disabled</th>
										<%if(request.getParameter("reported")!=null&&(request.getParameter("reported").equals("true")||request.getParameter("reported")!=null&&request.getParameter("reported").toString().equals("false")));
										else{
										%><th>Reported</th>
										<%} %>
											</tr>
									</thead>
									<tbody>
										<%
                	ArrayList<Student> csab_list=postgreSQLDatabase.registration.Query.getCsabStudentList();
                                Iterator<Student> iterator=csab_list.iterator();
                                while(iterator.hasNext()){
                    				Student current=iterator.next();
                    				if(request.getParameter("reported")!=null&&request.getParameter("reported").equals("true") &&!current.isReported())continue;
                    				if(request.getParameter("reported")!=null&&request.getParameter("reported").equals("false") &&current.isReported())continue;
                    					
                %>
										<tr>
											<td><center><%=current.getName() %></center></td>
											<td><center><%=current.getJee_main_rollno() %></center></td>
											<td><center><%=current.getState_eligibility()%></center></td>
											<td><center><%=current.getMobile()%></center></td>
											<td><center><%=current.getEmail() %></center></td>
											<td><center><%=current.getProgram_allocated()%></center></td>
											<td><center><%=current.getAllocated_category()%></center></td>
											<td><center><%=current.isPwd()%></center></td>
											<%if(request.getParameter("reported")!=null&&(request.getParameter("reported").equals("true")||request.getParameter("reported").equals("false")));
										else{
										%><td><center><%=current.isReported() %></center></td>
										<%} %>
											
										</tr>
										<%
                }
				%>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->

<script type="text/javascript">
window.print();
</script>
</body>
</html>