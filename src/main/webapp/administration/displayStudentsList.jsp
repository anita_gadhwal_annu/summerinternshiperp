<!DOCTYPE html>
<%@page import="postgreSQLDatabase.registration.Query"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="users.Student"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>IIITK | ERP</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet"
	href="../plugins/datatables/dataTables.bootstrap.css">
<!-- Theme style -->
<link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">

<style>
.example-modal .modal {
	position: relative;
	top: auto;
	bottom: auto;
	right: auto;
	left: auto;
	display: block;
	z-index: 1;
}

.example-modal .modal {
	background: transparent !important;
}
</style>



<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">

		<%@ include file="header.jsp"%>
		<!-- Left side column. contains the logo and sidebar -->
		<%@ include file="main-sidebar.jsp"%>
		<%@ page import="java.util.ArrayList"%>
		<%@ page import="java.util.Iterator"%>
		<%@ page import="users.Student"%>



		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Student <small> </small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
					<li><a href="#">Tables</a></li>
					<li class="active">Data tables</li>
				</ol>
			</section>


			<!--MODAL-->
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
				aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<h4 class="modal-title" id="myModalLabel">Student Details</h4>
						</div>
						<div class="modal-body">

							<table id="modal_table"
								class="table table-bordered table-striped">
								<tbody>

									<tr>
										<td>Registration Id</td>
										<td ><input class="form-control" 
											type="text" id="registration_id"></td>
									</tr>


									<tr>
										<td>Name</td>
										<td ><input type="text" class="form-control" id="name"></td>
									</tr>

									<tr>
										<td>First Name</td>
										<td><input type="text" class="form-control"  id="first_name"></td>
									</tr>

									<tr>
										<td>Middle Name</td>
										<td><input type="text" class="form-control"  id="middle_name"></td>
									</tr>

									<tr>
										<td>Last Name</td>
										<td><input type="text" class="form-control"  id="last_name"></td>
									</tr>

									<tr>
										<td>Category</td>
										<td><input type="text" class="form-control"  id="category"></td>
									</tr>

									<tr>
										<td>State</td>
										<td><input type="text" class="form-control"  id="state"></td>
									</tr>
									<tr>
										<td>Status</td>
										<td><input type="text" class="form-control"  id="status"></td>
									</tr>
									<tr>
										<td>Mobile</td>
										<td><input type="text" class="form-control"  id="mobile"></td>
									</tr>
									<tr>
										<td>Email</td>
										<td><input type="text" class="form-control"  id="email"></td>
									</tr>
									<tr>
										<td>Date Of Birth</td>
										<td><input type="text" class="form-control"  id="date_of_birth"></td>
									</tr>
									<tr>
										<td>Program Allocated</td>
										<td><input type="text" class="form-control"  id="program_allocated"></td>
									</tr>

	
									<tr>
									<td>Physically Disabled</td>
										<td>
										<div id="physically_disabled">
										   <input type="radio" name="pwd" id="isPwd" ><label>Yes</label>
										   <input type="radio" name="pwd" id="isNotPwd" ><label>No</label>
										   </div>
										</td>
</tr>

									<tr>
										<td>Gender</td>
										<td><input type="text" class="form-control"  id="gender"></td>
									</tr>

									<tr>
										<td>Guardian Name</td>
										<td><input type="text" class="form-control"  id="guardian_name"></td>
									</tr>

									<tr>
										<td>Guardian Contact</td>
										<td><input type="text" class="form-control"  id="guardian_contact"></td>
									</tr>

									<tr>
										<td>Guardian Email</td>
										<td><input type="text" class="form-control"  id="guardian_email"></td>
									</tr>

									<tr>
										<td>Guardian Address</td>
										<td><input type="text" class="form-control"  id="guardian_address"></td>
									</tr>

									<tr>
										<td>Father Name</td>
										<td><input type="text" class="form-control"  id="father_name"></td>
									</tr>

									<tr>
										<td>Father Contact</td>
										<td><input type="text" class="form-control"  id="father_contact"></td>
									</tr>

									<tr>
										<td>Mother Name</td>
										<td><input type="text" class="form-control"  id="mother_name"></td>
									</tr>

									<tr>
										<td>Mother Contact</td>
										<td><input type="text" class="form-control"  id="mother_contact"></td>
									</tr>

									<tr>
										<td>Nationality</td>
										<td><input type="text" class="form-control"  id="nationality"></td>
									</tr>

									<tr>
										<td>Permanent Address</td>
										<td><input type="text" class="form-control"  id="permanent_address"></td>
									</tr>
									<tr>
										<td>Local Address</td>
										<td><input type="text" class="form-control"  id="local_address"></td>
									</tr>

									<tr>
										<td>Hosteller</td>
										<td>
										<div id="hosteller">
										   <input type="radio" name="hostel" id="isHosteller"/><label>Yes</label>
										   <input type="radio" name="hostel" id="isNotHosteller" /><label>No</label>
										   </div>
										</td>
									</tr>

									<tr>
										<td>Hostel Address</td>
										<td><input type="text" class="form-control"  id="hostel_address"></td>
									</tr>


									<tr>
										<td>Semester</td>
										<td><input type="text" class="form-control"  id="semester"></td>
									</tr>
								</tbody>
							</table>

						</div>
						<div class="modal-footer">
						<button type="button" onclick="update()" class="btn btn-warning"
								data-dismiss="modal" id="update_data">Update</button>
							
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Close</button>

						</div>
					</div>
				</div>
			</div>

			<!--MODAL-->

<!-- Modal 2 begin-->
<!-- Modal -->

			<div class="modal fade" id="myModalToAdd" tabindex="-1" role="dialog"
				aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<h4 class="modal-title" id="myModalLabel">Student Details</h4>
						</div>
						<div class="modal-body">

							<table id="modal_table"
								class="table table-bordered table-striped">
								<tbody>

									<tr>
										<td>Name</td>
										<td><input type="text" id="insert_name" class="form-control"
											value=""></td>
									</tr>
									<tr>
										<td>First Name</td>
										<td><input type="text" id="insert_first_name"
											class="form-control" value=""></td>
									</tr>
									<tr>
										<td>Middle Name</td>
										<td><input type="text" id="insert_middle_name"
											class="form-control" value=""></td>
									</tr>
									<tr>
										<td>Last Name</td>
										<td><input type="text" id="insert_last_name"
											class="form-control" value=""></td>
									</tr>

									<tr>
										<td>Category</td>
										<td><input type="text" class="form-control" id="insert_category"></td>
									</tr>
										<td>State</td>
										<td><input type="text" class="form-control" id="insert_state"></td>
									</tr>
									<tr>
										<td>Phone Number</td>
										<td><input type="text" class="form-control" id="insert_phone_no"></td>
									</tr>
									<tr>
										<td>Email</td>
										<td><input type="text" class="form-control" id="insert_email"></td>
									</tr>
									<tr>
										<td>Date Of Birth</td>
										<td><input type="text" class="form-control"
											id="insert_date_of_birth"></td>
									</tr>
									<tr>
										<td>Program Allocated</td>
										<td><input type="text" class="form-control"
											id="insert_program_allocated"></td>
									</tr>
										<td>Status</td>
										<td><input type="text" class="form-control" id="insert_status"></td>
									</tr>
									<tr>
										<td>Physically Disabled</td>
										<td>
										<div id="insert_physically_disabled">
										   <input type="radio" name="pwd" id="yes" value="Yes">Yes
										   <input type="radio" name="pwd" id="no" value="No">No
										   </div>
										</td>
									</tr>
									<tr>
										<td>Gender</td>
										<td><input class="form-control" class="form-control"
											type="text" id="insert_gender"></td>
									</tr>
									<tr>
										<td>Nationality</td>
										<td><input type="text" class="form-control"
											id="insert_nationality"></td>
											<tr>
										<td>Guardian Name</td>
										<td><input type="text" class="form-control"
											id="insert_guardian_name"></td>
									</tr>
									<tr>
										<td>Guardian Contact</td>
										<td><input type="text" class="form-control"
											id="insert_guardian_contact"></td>
									</tr>
									<tr>
										<td>Guardian Email</td>
										<td><input type="text" class="form-control"
											id="insert_guardian_email"></td>
									</tr>
									<tr>
										<td>Guardian address</td>
										<td><input type="text" class="form-control"
											id="insert_guardian_address"></td>
									</tr>
									<tr>
										<td>Father Name</td>
										<td><input type="text" class="form-control"
											id="insert_father_name"></td>
									</tr><tr>
										<td>Father Contact</td>
										<td><input type="text" class="form-control"
											id="insert_father_contact"></td>
									</tr>
									<tr>
										<td>Mother Name</td>
										<td><input type="text" class="form-control"
											id="insert_mother_name"></td>
									</tr><tr>
										<td>Mother Contact</td>
										<td><input type="text" class="form-control"
											id="insert_mother_contact"></td>
									</tr>
									<tr>
										<td>Permanent Address</td>
										<td><input type="text" class="form-control"
											id="insert_permanent_address"></td>
									</tr><tr>
										<td>Local Address</td>
										<td><input type="text" class="form-control"
											id="insert_local_address"></td>
									</tr>
									<tr>
										<td>Hosteller</td>
										<td>
										<div id="insert_hosteller">
										   <input type="radio" name="hostel" id="insert_isHosteller" value="Yes">Yes
										   <input type="radio" name="hostel" id="insert_isNotHosteller" value="No">No
										   </div>
										</td>
									</tr>
									
									<tr>
										<td>Hostel Name</td>
										<td><input type="text" class="form-control"
											id="insert_hostel_address"></td>
									</tr>
									
								</tbody>
							</table>

						</div>
						<div class="modal-footer">
							<button type="button" onclick="addRegisteredStudent()" class="btn btn-danger"
								data-dismiss="modal" id="update_data">Add</button>
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Close</button>

						</div>
					</div>
				</div>
			</div>


<!--  Modal 2 end -->



			<!-- Main content -->
			<section class="content">
				<div class="row">
					<div class="col-xs-12">

						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Student List</h3>
								<button type="button" class="btn btn-block btn-default"
									data-toggle="modal" data-target="#myModalToAdd">Add
								
							</div>
							
							<!-- /.box-header -->
							<div class="box-body" style="overflow-x: scroll;">
								<table id="example1" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>View</th>
											<th>Delete</th>
											<th>Registration ID</th>
											<th>Student ID</th>
											<th>Username</th>
											<th>Name</th>
											<th>Program_allocated</th>
											<th>Semester</th>
											<th>Phone Number</th>
											<th>Email</th>
											
										</tr>
									</thead>
									<tbody>
									
										<%
											ArrayList<Student> studentErp_list =postgreSQLDatabase.admin.Query.getStudentsErpList();
											Iterator<Student> iterator = studentErp_list.iterator();
											while (iterator.hasNext()) {
												Student current = iterator.next();
										%>
										<tr>
										<td><button type="button"
													class="btn btn-block btn-default" data-toggle="modal"
													data-target="#myModal"
													onclick="displayProfile(<%=current.getRegistration_id() %>)">
													<i class="glyphicon glyphicon-eye-open"></i>
												</button></td>
											<td><button type="button"
										class="btn btn-block btn-danger"
											onclick="deleteRegisteredStudent(<%=current.getRegistration_id()%>)">
													<i class="glyphicon glyphicon-trash"></i>
												</button></td>

	                               <td><%=current.getRegistration_id()%></td>
	                                  <td><%=current.getStudent_id()%></td>
									<td><%=current.getUsername()%></td>
											<td><%=current.getName()%></td>
											<td><%=current.getProgram_allocated()%></td>
												<td><%=current.getSemester()%></td>
											<td><%=current.getMobile()%></td>
											<td><%=current.getEmail()%></td>
										</tr>
										<%
                }
				%>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<%@ include file="footer.jsp"%>
		<!-- Control Sidebar -->
		<%@ include file="control-sidebar.jsp"%>
		<!-- /.control-sidebar -->

		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

	<!-- jQuery 2.1.4 -->
	<script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<!-- Bootstrap 3.3.5 -->
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<!-- DataTables -->
	<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- SlimScroll -->
	<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="../plugins/fastclick/fastclick.min.js"></script>
	<!-- AdminLTE App -->
	<script src="../dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../dist/js/demo.js"></script>
	<!-- page script -->
	<script src="../dist/js/payment.js"></script>
	<script>
  $(function () {
    $("#example1").DataTable({
		"paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
	});
  });
  
  
  function displayProfile(registration_id){
		var xmlhttp;
		try{
			xmlhttp = new XMLHttpRequest();
		} catch (e){
			// Internet Explorer Browsers
			try{
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e){
					//Browser doesn't support ajax	
					alert("Your browser is unsupported");
				}
			}
		}	
		//var xmlhttp=new XMLHttpRequest();

		if(xmlhttp){	
			xmlhttp.onreadystatechange=function() {
				if (xmlhttp.readyState==4 && xmlhttp.status==200) {
		//	alert(xmlhttp.responseText);
					var data=JSON.parse(xmlhttp.responseText);
					document.getElementById("name").value=data.name;
					document.getElementById("first_name").value=data.first_name;
					document.getElementById("middle_name").value=data.middle_name;
					document.getElementById("last_name").value=data.last_name;
					document.getElementById("category").value=data.category;
					document.getElementById("state").value=data.state;
					document.getElementById("mobile").value=data.phone_number;
					document.getElementById("email").value=data.email;
					document.getElementById("date_of_birth").value=data.date_of_birth;
					document.getElementById("program_allocated").value=data.program_allocated;
					if(data.physically_disabled==true){
						document.getElementById("isPwd").checked=true;
						document.getElementById("isNotPwd").checked=false;
					}
				
					else{
						document.getElementById("isPwd").checked=false;
						document.getElementById("isNotPwd").checked=true;
					}
						
					if(data.hosteller==true){
						document.getElementById("isHosteller").checked=true;
						document.getElementById("isNotHosteller").checked=false;
					}
						
						else{
							document.getElementById("isHosteller").checked=false;
							document.getElementById("isNotHosteller").checked=true;
						}
					document.getElementById("hostel_address").value=data.hostel_address;
					document.getElementById("semester").value=data.semester;
					document.getElementById("gender").value=data.gender;
					document.getElementById("guardian_name").value=data.guardian_name;
					document.getElementById("guardian_contact").value=data.guardian_contact;
					document.getElementById("guardian_email").value=data.guardian_email;
					document.getElementById("guardian_address").value=data.guardian_address;
					document.getElementById("father_name").value=data.father_name;
					document.getElementById("father_contact").value=data.father_contact;
					document.getElementById("mother_name").value=data.father_name;
					document.getElementById("mother_contact").value=data.mother_contact;
					document.getElementById("permanent_address").value=data.permanent_address;
					document.getElementById("local_address").value=data.local_address;
					document.getElementById("registration_id").value=data.registration_id;
					document.getElementById("nationality").value=data.nationality;
					//document.getElementById("reported").innerHTML=data.reported;
					document.getElementById("registration_id").innerHTML=data.registration_id;

					document.getElementById("update_data").setAttribute("onclick","javascript:update("+registration_id+")");		
									
				}
				if(xmlhttp.status == 404)
					alert("Could not connect to server");
			}
			xmlhttp.open("POST","../RegisteredStudent?action=display&registration_id="+registration_id,true);
			
			xmlhttp.send();
		}
		return false;
  }
  
  
  function update(registration_id){
		var xmlhttp;
		try{
			xmlhttp = new XMLHttpRequest();
		} catch (e){
			// Internet Explorer Browsers
			try{
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e){
					//Browser doesn't support ajax	
					alert("Your browser is unsupported");
				}
			}
		}	
		//var xmlhttp=new XMLHttpRequest();


		if(xmlhttp){
			var data= new FormData();
			data.append("registration_id",registration_id);
			data.append("name",document.getElementById("name").value);
			data.append("first_name",document.getElementById("first_name").value);
			data.append("middle_name",document.getElementById("middle_name").value);
			data.append("last_name",document.getElementById("last_name").value);
			data.append("category",document.getElementById("category").value);
			data.append("state",document.getElementById("state").value);
			data.append("phone_number",document.getElementById("mobile").value);
			data.append("email",document.getElementById("email").value);
			data.append("date_of_birth",document.getElementById("date_of_birth").value);
			data.append("program_allocated",document.getElementById("program_allocated").value);
			data.append("status",document.getElementById("status").value);
			if(document.getElementById("isPwd").checked)
			data.append("pwd",true);
				else
			data.append("pwd",false);
				
				data.append("gender",document.getElementById("gender").value);
				data.append("nationality",document.getElementById("nationality").value);
			data.append("guardian_name",document.getElementById("guardian_name").value);
			data.append("guardian_contact",document.getElementById("guardian_contact").value);
			data.append("guardian_email",document.getElementById("guardian_email").value);
			data.append("guardian_address",document.getElementById("guardian_address").value);
			
			data.append("father_name",document.getElementById("father_name").value);
			data.append("father_contact",document.getElementById("father_contact").value);
			data.append("mother_contact",document.getElementById("mother_contact").value);
			data.append("mother_name",document.getElementById("mother_name").value);
			data.append("permanent_address",document.getElementById("permanent_address").value);
			data.append("local_address",document.getElementById("local_address").value);
			data.append("hostel_address",document.getElementById("hostel_address").value);
			if(document.getElementById("isHosteller").checked)
				data.append("hosteller",true);
				else
					data.append("hosteller",false);
			
			//document.getElementById("reported").value);
			xmlhttp.onreadystatechange=function() {
				if (xmlhttp.readyState==4 && xmlhttp.status==200) {
					alert(xmlhttp.responseText);
				}
				if(xmlhttp.status == 404)
					alert("Could not connect to server");
			}
			xmlhttp.open("POST","../RegisteredStudent?action=update",true);
			//xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			var outputLog = {}, iterator =data.entries(), end = false;
			while(end == false) {
			  var item = iterator.next();
			  if(item.value!=undefined) {
			      outputLog[item.value[0]] = item.value[1];
			  } else if(item.done==true) {
			      end = true;
			  }
			   }
			console.log(outputLog);
			                              
			                xmlhttp.send(data);
			            }
		return false;

	}

  
  
  
  function deleteRegisteredStudent(registration_id){
	  var xmlhttp;
		try{
			xmlhttp = new XMLHttpRequest();
		} catch (e){
			// Internet Explorer Browsers
			try{
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e){
					//Browser doesn't support ajax	
					alert("Your browser is unsupported");
				}
			}
		}	
		//var xmlhttp=new XMLHttpRequest();

		if(xmlhttp){	
			xmlhttp.onreadystatechange=function() {
				if (xmlhttp.readyState==4 && xmlhttp.status==200) {
					window.location.reload();
					//alert(xmlhttp.responseText);
							}
				if(xmlhttp.status == 404)
					alert("Could not connect to server");
			}
			xmlhttp.open("POST","../RegisteredStudent?action=delete&registration_id="+registration_id,true);
			//xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			xmlhttp.send();
		}
		return false;
  }	
  
  function addRegisteredStudent(){
		var xmlhttp;
		try{
			xmlhttp = new XMLHttpRequest();
		} catch (e){
			// Internet Explorer Browsers
			try{
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e){
					//Browser doesn't support ajax	
					alert("Your browser is unsupported");
				}
			}
		}	
		//var xmlhttp=new XMLHttpRequest();


		if(xmlhttp){
			var data= new FormData();
			data.append("name",document.getElementById("insert_name").value);
			data.append("first_name",document.getElementById("insert_first_name").value);
			data.append("middle_name",document.getElementById("insert_middle_name").value);
			data.append("last_name",document.getElementById("insert_last_name").value);
			data.append("category",document.getElementById("insert_category").value);
			data.append("state",document.getElementById("insert_state").value);
			data.append("phone_number",document.getElementById("insert_phone_no").value);
			data.append("email",document.getElementById("insert_email").value);
			data.append("date_of_birth",document.getElementById("insert_date_of_birth").value);
			data.append("program_allocated",document.getElementById("insert_program_allocated").value);
			data.append("status",document.getElementById("insert_status").value);
			data.append("guardian_name",document.getElementById("insert_guardian_name").value);
			data.append("guardian_contact",document.getElementById("insert_guardian_contact").value);
			data.append("guardian_email",document.getElementById("insert_guardian_email").value);
			data.append("guardian_address",document.getElementById("insert_guardian_address").value);
			data.append("father_name",document.getElementById("insert_father_name").value);
			data.append("father_contact",document.getElementById("insert_father_contact").value);
			data.append("mother_name",document.getElementById("insert_mother_name").value);
			data.append("mother_contact",document.getElementById("insert_mother_contact").value);
			
			data.append("permanent_address",document.getElementById("insert_permanent_address").value);
			data.append("local_address",document.getElementById("insert_local_address").value);
			data.append("hostel_address",document.getElementById("insert_hostel_address").value);
			
			if(data.physically_disabled==true)
			data.append("pwd",document.getElementById("insert_physically_disabled").value);
			else
				data.append("pwd",document.getElementById("insert_physically_disabled").value);
			if(data.hosteller==true)
				data.append("hosteller",document.getElementById("insert_isHosteller").value);
				else
					data.append("hosteller",document.getElementById("insert_isNotHosteller").value);

			data.append("gender",document.getElementById("insert_gender").value);
			data.append("nationality",document.getElementById("insert_nationality").value);
			xmlhttp.onreadystatechange=function() {
				if (xmlhttp.readyState==4 && xmlhttp.status==200) {
					//alert(xmlhttp.responseText);
					var response=JSON.parse(xmlhttp.responseText);
					if(response.success==true) {
						alert("hello"+succes);
					window.location.reload();	
					}
					else{
						alert(JSON.stringify(response.error));
					}
					
				}
				if(xmlhttp.status == 404)
					alert("Could not connect to server");
			}
			xmlhttp.open("POST","../RegisteredStudent?action=add",true);
			//xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			var outputLog = {}, iterator =data.entries(), end = false;
			while(end == false) {
			  var item = iterator.next();
			  if(item.value!=undefined) {
			      outputLog[item.value[0]] = item.value[1];
			  } else if(item.done==true) {
			      end = true;
			  }
			   }
			console.log(outputLog);
			                              
			                xmlhttp.send(data);
			            }
		return false;

	}

  
</script>
</body>
</html>