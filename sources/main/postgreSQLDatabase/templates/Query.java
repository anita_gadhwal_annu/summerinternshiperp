/**
 * 
 */
package postgreSQLDatabase.templates;

import java.lang.reflect.Array;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;

import org.apache.poi.util.SystemOutLogger;

import exceptions.IncorrectFormatException;
import postgreSQLDatabase.onlineTest.Answer;
import postgreSQLDatabase.templates.Template;
import settings.database.PostgreSQLConnection;
import users.Student;

/**
 * 
 * @author manisha pc and Anita
 *
 */
public class Query {
	static Connection conn;

	public static void main(String[] args) throws SQLException {

	}

	public static void addTemplate(String title, long file, Long author, ArrayList<String> tag) throws SQLException {
		PreparedStatement proc = PostgreSQLConnection.getConnection()
				.prepareStatement("SELECT public.\"addTemplates\"(?,?,?,?);");

		proc.setString(1, title);
		proc.setLong(2, file);
		proc.setLong(3, author);
		proc.setArray(4, PostgreSQLConnection.getConnection().createArrayOf("text", tag.toArray()));
		proc.executeQuery();
	}

	public static ArrayList<Template> getAllTemplates() {
		ArrayList<Template> template_list = null;
		try {
			PreparedStatement proc = PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"getAllTemplates\"();");

			ResultSet rs = proc.executeQuery();

			template_list = new ArrayList<Template>();
			while (rs.next()) {
				Template templates = new Template();
				templates.setFile(rs.getLong("file"));
				templates.setTitle(rs.getString("title"));
				templates.setAuthor(rs.getLong("author"));
				templates.setId(rs.getLong("id"));
               postgreSQLDatabase.file.File file =new postgreSQLDatabase.file.File();
               templates.setFile_wrapper(postgreSQLDatabase.file.Query.getAllFileData(rs.getLong("file")));
				ResultSet as = rs.getArray("tags").getResultSet();
				ArrayList<String> tags = new ArrayList<String>();
				while (as.next()) {
					tags.add(as.getString(2));
					 
				}

				templates.setTags(tags);

				template_list.add(templates);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return template_list;

	}

	public static Template getTemplates(long id) {
		Template templates=null;
		try {
			PreparedStatement proc = PostgreSQLConnection.getConnection().prepareStatement("SELECT * from public.\"getTemplates\"(?);");
			proc.setLong(1, id);
			System.out.println(proc);
			ResultSet rs = proc.executeQuery();
           rs.next();
			
				 templates = new Template();
				 templates.setId(id);
				templates.setFile(rs.getLong("file"));
				templates.setTitle(rs.getString("title"));
				templates.setAuthor(rs.getLong("author"));
				templates.setFile_wrapper(postgreSQLDatabase.file.Query.getAllFileData(rs.getLong("file")));
				ResultSet as = rs.getArray("tags").getResultSet();
				ArrayList<String> tags = new ArrayList<String>();
				while (as.next()) {
					tags.add(as.getString(1));
					// System.out.println(as.getString(1));

					templates.setTags(tags);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return templates;

	}

	public static void addTags(String tagname, String users, String attributes) {

		try {
			PreparedStatement proc = PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT public.\"addTags\"(?,?,?);");
			proc.setString(1, tagname);
			proc.setString(2, users);
			proc.setString(3, attributes);
			proc.executeQuery();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static Tags getTags(String tag_name) {
		Tags tag = new Tags();
		try {
			PreparedStatement proc = PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"getTags\"(?);");
			proc.setString(1, tag_name);
			System.out.println(proc);
			ResultSet rs = proc.executeQuery();
			if(!rs.next())return null;
				
				tag.setUsers(rs.getString("users"));
				tag.setAttributes(rs.getString("attributes"));
                tag.setTagname(rs.getString("tagname"));
                tag.setId(rs.getLong("id"));
			
		}

		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tag;

	}

	public static long getFileId(String file_name) {
		long file_id = 0;
		try {
			PreparedStatement proc = PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"getFileId\"(?);");
			proc.setString(1, file_name);
			ResultSet rs = proc.executeQuery();
			rs.next();
			file_id = rs.getLong(1);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return file_id;

	}

	public static Student getStudentByStudentId(String std_id) {
		Student current=null;
		try {
			PreparedStatement proc = PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"getStudentListById\"(?);");
			proc.setString(1, std_id);
			ResultSet rs = proc.executeQuery();
			rs.next();
			current=new Student(rs);
		return(current);	
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IncorrectFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return current;
	}
	public static Long deleteTemplate(long template_id ) throws SQLException {
		PreparedStatement proc = PostgreSQLConnection.getConnection()
				.prepareStatement("SELECT public.\"deleteTemplate\"(?);");
		proc.setLong(1, template_id);
		System.out.println(proc.toString());
		ResultSet rs = proc.executeQuery();
		rs.next();
		System.out.println(rs.getLong(1));
		return rs.getLong(1);
	}
	
	
	public static ArrayList<Tags> getAllTags(){
	
		ArrayList<Tags> tags_list = null;
		try {
			PreparedStatement proc = PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"getAllTags\"();");
		
			ResultSet rs = proc.executeQuery();
			tags_list = new ArrayList<Tags>();
			while (rs.next()) {
				Tags current=new Tags();
				current.setTagname(rs.getString("tagname"));
				current.setAttributes(rs.getString("attributes"));
				current.setUsers(rs.getString("users"));
				current.setId(rs.getLong("id"));
				tags_list.add(current);

				
			//System.out.println(rs.getString("users")+"hello/n"+rs.getString("attributes"));
			}
			return tags_list;
			}

		 catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tags_list;

	}
	
	
public static void deleteTags(long tag_id){
	
	try {
		PreparedStatement proc = PostgreSQLConnection.getConnection()
				.prepareStatement("SELECT public.\"deleteTags\"(?);");
   
      proc.setLong(1,tag_id);
      proc.executeQuery();
   System.out.println("delete");
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	

}
	
	
	
	
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	


