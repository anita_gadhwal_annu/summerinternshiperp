package actions.document;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.tomcat.util.http.fileupload.RequestContext;

/**
 * Servlet implementation class DocumentUpload
 */
@WebServlet("/DocumentUpload")
@MultipartConfig

public class DocumentUpload extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final int MEMORY_THRESHOLD   = 1024 * 1024 * 3;  // 3MB
	private static final int MAX_FILE_SIZE      = 1024 * 1024 * 40; // 40MB
	private static final int MAX_REQUEST_SIZE   = 1024 * 1024 * 50; // 50MB
	private static final String UPLOAD_DIRECTORY = "upload";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DocumentUpload() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter writer = response.getWriter();
		
		//		writer.write(text);
		if (!ServletFileUpload.isMultipartContent(request)) {
			// if not, we stop here
			writer.println("Error: Form must has enctype=multipart/form-data.");
			writer.flush();
			return;
		}

		// configures upload settings
		DiskFileItemFactory factory = new DiskFileItemFactory();
		// sets memory threshold - beyond which files are stored in disk
		factory.setSizeThreshold(MEMORY_THRESHOLD);
		// sets temporary location to store files
		factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
		String fileName = null;
		InputStream inputStream = null;
		String document_name=null;
		ServletFileUpload upload = new ServletFileUpload(factory);
		try {
			// parses the request's content to extract file data
			@SuppressWarnings("unchecked")
			List<FileItem> formItems = upload.parseRequest(request);

			if (formItems != null && formItems.size() > 0) {
				// iterates over form's fields
				for (FileItem item : formItems) {
					// processes only fields that are not form fields
					if (!item.isFormField()) {
						fileName = new File(item.getName()).getName();
						//           String file=String.valueOf((new Date().getTime()))+ fileName;
						//         File storeFile = new File(uploadPath + File.separator +fileName);

						// saves the file on disk
						 inputStream =item.getInputStream();
						// item.write(storeFile);
					}
					else{
						if(item.getFieldName().equals("document_name")){
							 document_name=item.getString();
						}
								
					}
				}
			}
		} catch (Exception ex) {
			request.setAttribute("message",
					"There was an error: " + ex.getMessage());
		}

		// sets maximum size of upload file
		upload.setFileSizeMax(MAX_FILE_SIZE);

		// sets maximum size of request (include file + form data)
		upload.setSizeMax(MAX_REQUEST_SIZE);
		long reg_id=Long.parseLong(request.getParameter("reg_id"));
		//upload file to FTP
		String filePath = "uploads" + "/" + "student_" +reg_id	;
		System.out.println(filePath);
		ftp.FTPUpload.createDirectory("uploads");
		ftp.FTPUpload.createDirectory(filePath);

		
		
		
		postgreSQLDatabase.file.File file_wrapper = new postgreSQLDatabase.file.File();
		file_wrapper.setDirectory("uploads/student_"+reg_id);
		file_wrapper.setAuthor(Long.parseLong((request.getSession().getAttribute("erpId")).toString()));
		String name[] = fileName.split("\\.");

		file_wrapper.setExtension(name[1]);

		file_wrapper.setFile_name(fileName);
		
	
		try {
			// add reference to database
			long file_id = postgreSQLDatabase.file.Query.addNewFile(file_wrapper);
			
			postgreSQLDatabase.documents.Documents document = new postgreSQLDatabase.documents.Documents();
			document.setOwner_id(reg_id);
			document.setFile_id(file_id);
			document.setDocument_name(document_name);
			postgreSQLDatabase.documents.Query.addDocument(document);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//Upoad to FTP
		ftp.FTPUpload.uploadFile(inputStream, filePath + "/" + fileName);
		
		response.sendRedirect("office/documents.jsp?reg_id="+reg_id);
	}
		
		

	}





